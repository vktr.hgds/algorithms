package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;
/**
 * time complexity: DIVIDE & CONQUER algorithm (divide into subproblems)
 * divide:
 *      worst case: O(log(n)), best case: O(log(n))
 * merging items: read each item for each subarray to merge it
 *      worst case: O(n), best case: O(n)
 *
 * overall:
 *      O(n * log(n))
 *
 * space complexity:
 *      worst case: O(k) - maximum value in array
 */

public class CountingSort implements Sorting {
    private int[] values;

    public CountingSort(int[] values) {
        this.values = values;
    }
    public CountingSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    @Override
    public void sort() {
        int[] countElements = new int[findMax(values) + 1];
        for (int value : values) countElements[value]++;

        int k = 0;
        for (int i = 0; i < countElements.length; ++i) {
            if (countElements[i] == 0) continue;
            int j = countElements[i];
            while (j != 0) {
                values[k++] = i;
                j--;
            }
        }
    }

    public int[] getValues() {
        return values;
    }

    private int findMax(int[] values) {
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < values.length; ++i)
            if (values[i] > max)
                max = values[i];

        return max;
    }
}
