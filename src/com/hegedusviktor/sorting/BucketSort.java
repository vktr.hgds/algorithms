package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * time complexity: more buckets means faster run, but more space needed
 * distribution:
 *      worst case: O(n), best case: O(n)
 * iterating buckets: we have k buckets
 *      worst case: O(k), best case: O(k)
 * sorting buckets: we have k buckets
 *      worst case: O(n2) (eg. bubblesort), best case: O(1)
 * overall:
 *      O(n+k) - linear, O(n2) - quadratic
 *
 * space complexity:
 *      space: O(n+k)
 */

public class BucketSort implements Sorting {
    private int[] values;

    public BucketSort(int[] values) {
        this.values = values;
    }
    public BucketSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    @Override
    public void sort() {
        int numberOfBuckets = 4;
        List<List<Integer>> buckets = new LinkedList<>();
        for(int i = 0; i < numberOfBuckets; i++)
            buckets.add(new LinkedList<>());

        for (int item : values)
            buckets.get(item / numberOfBuckets).add(item);

        int i = 0;
        for (List<Integer> bucket : buckets) {
            Collections.sort(bucket);
            for (int item : bucket) {
                values[i++] = item;
            }
        }
    }

    public int[] getValues() {
        return values;
    }
}
