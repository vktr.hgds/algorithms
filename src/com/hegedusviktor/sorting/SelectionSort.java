package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;

/**
 * complexity:
 * passes:
 *      worst case: O(n), best case: O(n)
 * min find:
 *      worst case: O(n), best case: O(n)
 *
 * overall: O(n2) - quadratic
 */

public class SelectionSort implements Sorting {
    private int[] values;

    public SelectionSort(int[] values) {
        this.values = values;
    }
    public SelectionSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    public void sort() {
        for (int i = 0; i < values.length; ++i) {
            CommonOperations.traditionalSwap(values, findMinIndex(values, i), i);
        }
    }

    public int[] getValues() {
        return values;
    }

    private int findMinIndex(int[] values, int i) {
        int minIndex = i;
        for (int j = i; j < values.length; ++j)
            if (values[j] < values[minIndex]) minIndex = j;

        return minIndex;
    }
}
