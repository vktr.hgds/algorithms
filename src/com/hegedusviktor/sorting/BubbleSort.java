package com.hegedusviktor.sorting;
import com.hegedusviktor.common.CommonOperations;

public class BubbleSort implements Sorting {
    private int[] values;

    public BubbleSort(int[] values) {
        this.values = values;
    }
    public BubbleSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    public void sort() {
        for (int i = 0; i < values.length - 1; ++i) {
            for (int j = 0; j < values.length - i - 1; ++j) {
                if (values[j] > values[j + 1]) {
                    CommonOperations.unusualSwap(values, j, j+1);
                }
            }
        }
    }

    public int[] getValues() {
        return values;
    }
}
