package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;

/**
 * complexity:
 * iteration:
 *      worst case: O(n), best case: O(n)
 * shift items:
 *      worst case: O(n), best case: O(1)
 *
 * overall:
 *      worst case: O(n2) - quadratic, best case: O(n) - linear
 */

public class InsertionSort implements Sorting {
    private int[] values;

    public InsertionSort(int[] values) {
        this.values = values;
    }
    public InsertionSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    public void sort() {
        for (int i = 1; i < values.length; ++i) {
            int current = values[i];
            int j = i - 1;
            while (j >= 0 && values[j] > current) {
                values[j + 1] = values[j];
                j--;
            }
            values[j + 1] = current;
        }
    }

    public int[] getValues() {
        return values;
    }
}
