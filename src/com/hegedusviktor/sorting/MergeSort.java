package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;

/**
 * time complexity: DIVIDE & CONQUER algorithm (divide into subproblems)
 * divide:
 *      worst case: O(log(n)), best case: O(log(n))
 * merging items: read each item for each subarray to merge it
 *      worst case: O(n), best case: O(n)
 *
 * overall:
 *      O(n * log(n))
 *
 * space complexity:
 *      worst case: O(n), best case: O(n), each subarray combined has the length of the original array
 */

public class MergeSort {
    private int[] values;

    public MergeSort(int[] values) {
        this.values = values;
    }
    public MergeSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    public void sort(int[] array) {
        if (array.length < 2) return;

        // Divide into half
        int middle = array.length / 2;
        int[] left = new int[middle];
        for (int i = 0; i < left.length; i++) {
            left[i] = array[i];
        }

        int[] right = new int[array.length - middle];
        for (int j = 0; j < right.length; j++) {
            right[j] = array[j + middle];
        }

        // Sort each half
        sort(left);
        sort(right);

        // Merge result
        merge(left, right, array);
    }

    public int[] getValues() {
        return values;
    }

    private void merge(int[] left, int[] right, int[] result) {
        int i = 0, j = 0, k = 0;
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                result[k++] = left[i++];
            } else {
                result[k++] = right[j++];
            }
        }

        while (i < left.length)
            result[k++] = left[i++];

        while (j < right.length)
            result[k++] = right[j++];
    }
}
