package com.hegedusviktor.sorting;

import com.hegedusviktor.common.CommonOperations;

/**
 * complexity:
 * how many partitioning:
 *      worst case: O(log(n)), best case: O(n)
 * partitioning:
 *      worst case: O(n), best case: O(n)
 *
 * overall: best case: O(n(log(n))), worst case: O(n2) (quadratic)
 * space complexity - O(log(n)), worst O(n)
 */

public class QuickSort {
    private int[] values;

    public QuickSort(int[] values) {
        this.values = values;
    }
    public QuickSort(int length) {
        this.values = CommonOperations.generateRandomValues(length);
    }

    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int start, int end) {
        if (start >= end) return;

        // Partitioning: select pivot (smaller then this goes left, greater goes right) - 2 groups
        int boundary = partition(array, start, end);
        sort(array, start, boundary - 1);
        sort(array, boundary + 1, end);
    }

    public int[] getValues() {
        return values;
    }

    private int partition(int[] array, int start, int end) {
        int pivot = array[end];
        int boundary = start - 1;

        for (int i = start; i <= end; i++) {
            if (array[i] <= pivot) CommonOperations.traditionalSwap(array, i, ++boundary);
        }

        return boundary;
    }

}
