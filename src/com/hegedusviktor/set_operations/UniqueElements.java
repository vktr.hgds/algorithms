package com.hegedusviktor.set_operations;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class UniqueElements {
    private Set<String> uniqIds;

    public UniqueElements() {
        uniqIds = new HashSet<>();
    }

    public Set<String> getUniqIds() {
        return uniqIds;
    }

    public Set<String> findAll(String path) {
        try {
            File patientIds = new File(path);
            Scanner myReader = new Scanner(patientIds);

            while (myReader.hasNextLine())
                uniqIds.add(myReader.nextLine());

        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        }

        return uniqIds;
    }

    public void print() {
        for (String id : uniqIds)
            System.out.println(id);
    }

    public static List<Integer> removeDup(int[] arr) {
        List<Integer> uniqVals = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != arr[i + 1]) {
                uniqVals.add(arr[i]);
            }
        }

        return uniqVals;
    }
}
