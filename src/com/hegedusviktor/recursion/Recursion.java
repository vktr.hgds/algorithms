package com.hegedusviktor.recursion;

public class Recursion {

    public static int uniquePaths(int n, int m) {
        if (n == 1 || m == 1) return 1;
        return uniquePaths(n - 1, m) + uniquePaths(n, m - 1);
    }
}
