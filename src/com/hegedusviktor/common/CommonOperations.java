package com.hegedusviktor.common;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

public class CommonOperations {
    public static void print(int[] values) {
        String result = Arrays.stream(values)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" - "));

        System.out.println(result);
    }

    public static int[] generateRandomValues(int length) {
        Random rand = new Random();
        int[] arr = new int[length];

        for (int i = 0; i < length; ++i) arr[i] = rand.nextInt(30);

        return arr;
    }

    public static void traditionalSwap(int[] array, int firstIdx, int secondIdx) {
        int tmp = array[firstIdx];
        array[firstIdx] = array[secondIdx];
        array[secondIdx] = tmp;
    }

    public static void unusualSwap(int[] array, int firstIdx, int secondIdx) {
        array[firstIdx] = array[firstIdx] + array[secondIdx];
        array[secondIdx] = array[firstIdx] - array[secondIdx];
        array[firstIdx] = array[firstIdx] - array[secondIdx];
    }
}
