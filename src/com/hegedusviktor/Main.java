package com.hegedusviktor;

import com.hegedusviktor.data_structures.binary_tree.BinaryTree;
import com.hegedusviktor.data_structures.disjoint_set.PathCompression;
import com.hegedusviktor.data_structures.disjoint_set.ProvinceUnion;
import com.hegedusviktor.queue.MovingAverage;
import com.hegedusviktor.queue.MovingAverageAlternative;
import com.hegedusviktor.set_operations.UniqueElements;
import com.hegedusviktor.string_manipulation.StringOccurrences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // CountingSort sorting = new CountingSort(8);
        // CommonOperations.print((sorting.getValues()));
        // sorting.sort();
        // CommonOperations.print((sorting.getValues()));

        // ISearch search = new ExponentialSearch(10, 10);
        // System.out.println(search.search());
        // CommonOperations.print(((ExponentialSearch) search).getValues());

        //StringManipulation stringManipulation = new StringManipulation("hello hallo kuku");
        //System.out.println(stringManipulation.countVowels());
        //System.out.println(stringManipulation.reverseString());
        //System.out.println(stringManipulation.reverseWords());

        //StringManipulation stringManipulation2 = new StringManipulation("ABCDEE");
        //System.out.println(stringManipulation2.isRotation("EEABCD", 2));
        //System.out.println(stringManipulation2.removeDuplicates());
        //System.out.println(stringManipulation2.mostRepeatedCharacter());

        //StringManipulation stringManipulation3 = new StringManipulation("  trees    are        beautiful  l");
        //System.out.println(stringManipulation3.capitalize());

        //StringManipulation stringManipulation4 = new StringManipulation("abac56caba");
        //System.out.println(stringManipulation4.isPalindrome());

        //try {
        //    StackParentheses.printValid();
        //} catch (Exception e) {
        //    e.printStackTrace();
        //}

        PathCompression uf = new PathCompression(7);
        uf.union(0, 1);
        uf.union(1, 2);
        uf.union(1, 3);
        uf.union(4, 5);
        uf.union(4, 6);
        uf.print();

        // 1-2-5-6-7 3-8-9-4
        uf.union(1, 5);
        uf.print();

        int[][] arr = {
            {1,0,0,1},
            {0,1,1,0},
            {0,1,1,1},
            {1,0,1,1}
        };
        System.out.println(ProvinceUnion.findCircleNum(arr));
        System.out.println();

        UniqueElements ue = new UniqueElements();
        ue.findAll("/Users/hegedusviktor/Courses/Algorithms/src/com/hegedusviktor/patient_ids.txt");
        System.out.println("Number of unique ids: " + ue.getUniqIds().size());
        ue.print();

        ArrayList<String> strings = new ArrayList<>();
        strings.add("aba");
        strings.add("baba");
        strings.add("aba");
        strings.add("xzxb");

        ArrayList<String> queries = new ArrayList<>();
        queries.add("aba");
        queries.add("xzxb");
        queries.add("ab");

        System.out.println(StringOccurrences.matchingStrings(strings, queries));

        System.out.println(StringOccurrences.matchingStrings(
                "/Users/hegedusviktor/Courses/Algorithms/src/com/hegedusviktor/strings.txt",
                "/Users/hegedusviktor/Courses/Algorithms/src/com/hegedusviktor/queries.txt")
        );

        MovingAverage mv = new MovingAverage(30, 23, 22, 15);
        mv.printAverage();
        mv.add(22);
        mv.printAverage();
        mv.add(31);
        mv.printAverage();
        mv.add(43);
        mv.printAverage();
        System.out.println(mv.getValues());

        BinaryTree bt = new BinaryTree();
        bt.add(4);
        bt.add(2);
        bt.add(3);
        bt.add(0);
        bt.add(7);
        bt.add(5);
        bt.add(9);
        bt.add(-2);
        bt.add(-1);
        bt.preOrder(bt.root);
        System.out.println();
        bt.inOrder(bt.root);
        System.out.println();
        bt.postOrder(bt.root);

        int[] arr1 = {1, 3, 7, 9, 10, 4, 8, 19};
        int[] arr2 = {4, 2, 7, 10, 9, 1, 12, 15, 19};
        Set<Integer> intersection = new HashSet<>();

        for (int k : arr1) {
            for (int i : arr2) {
                if (k == i) intersection.add(k);
            }
        }

        System.out.println();
        intersection.stream().sorted().forEach(System.out::println);

        MovingAverageAlternative maa = new MovingAverageAlternative(1, 2, 3, 4, 11);
        System.out.println();
        System.out.println(maa.calculateAvg());
        maa.add(14);
        System.out.println(maa.calculateAvg());
    }
}
