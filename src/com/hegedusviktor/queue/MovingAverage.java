package com.hegedusviktor.queue;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class MovingAverage {
    private Queue<Double> values;
    private static final int AVG_VALUES_COUNT = 4;

    public MovingAverage(double... numbers) {
        this.values = new LinkedList<>();
        if (numbers.length == 0) return;

        for (Double number : numbers)
            this.values.add(number);
    }

    public void add(double number) {
        if (values.size() < AVG_VALUES_COUNT) {
            values.add(number);
            return;
        }

        values.poll();
        values.add(number);
    }

    public void printAverage() {
        if (values.size() < AVG_VALUES_COUNT) {
            System.out.println("At least 4 numbers should be added.");
            return;
        }

        double avg = values.stream()
                .mapToDouble(value -> value)
                .average()
                .orElse(Double.NaN);

        System.out.println(avg);
    }

    public Queue<Double> getValues() {
        return values;
    }
}
