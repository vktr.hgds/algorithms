package com.hegedusviktor.queue;

import java.util.LinkedList;
import java.util.Queue;

public class MovingAverageAlternative {
    private Queue<Double> values;
    private static final int MAX_RANGE = 4;

    public MovingAverageAlternative(double ...values) {
        this.values = new LinkedList<>();
        int start = values.length > MAX_RANGE ? values.length - MAX_RANGE : 0;

        for (int i = start; i < values.length; i++) {
            this.values.add(values[i]);
        }
    }

    public void add(double element) {
        if (values.size() < MAX_RANGE) {
            values.add(element);
            return;
        }

        values.poll();
        values.add(element);
    }

    public double calculateAvg() {
        double sum = 0;
        for (Double value : values) {
            sum += value;
        }
        return sum / values.size();
    }
}
