package com.hegedusviktor.data_structures.linked_list;

public class DoubleLinkedList {
    public Node head, tail = null;
    public int size = 0;

    public void add(int data) {
        Node node = new Node(data);
        node.next = null;

        if (head == null) {
            head = node;
        } else {
            Node last = head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = node;
        }
        size++;
    }

    public void print() {
        Node tmp = head;

        while (tmp != null) {
            System.out.println(tmp.data);
            tmp = tmp.next;
        }
    }
}
