package com.hegedusviktor.data_structures.linked_list;

public class Node {
    public int data;
    public Node next;
    public Node prev;

    public Node(int data) {
        this.data = data;
    }
}
