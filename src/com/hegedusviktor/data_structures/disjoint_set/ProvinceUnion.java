package com.hegedusviktor.data_structures.disjoint_set;

import java.util.HashSet;
import java.util.Set;

public class ProvinceUnion {
    public static int findCircleNum(int[][] isConnected) {
        int provinceCount = 0;
        Set<Integer> visited = new HashSet<>();

        for (int i = 0; i < isConnected.length; i++) {
            if (visited.contains(i)) continue;
            dfs(i, isConnected, visited);
            provinceCount++;
        }

        return provinceCount;
    }

    private static void dfs(int i, int[][] isConnected, Set<Integer> visited) {
        if (visited.contains(i)) return;

        visited.add(i);
        for (int j = 0; j < isConnected.length; j++) {
            if (isConnected[i][j] == 0) continue;
            dfs(j, isConnected, visited);
        }
    }
}
