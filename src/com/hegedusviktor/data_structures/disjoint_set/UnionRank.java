package com.hegedusviktor.data_structures.disjoint_set;

public class UnionRank {
    private int[] data;
    private int[] rank;

    public UnionRank(int size) {
        data = new int[size];
        rank = new int[size];

        for (int i = 0; i < data.length; i++) {
            data[i] = i;
            rank[i] = 1;
        }
    }

    public int find(int x) {
        while (x != data[x]) {
            x = data[x];
        }
        return x;
    }

    public void union(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);

        if (rootX == rootY) return;

        if (rank[rootX] < rank[rootY]) {
            data[rootY] = rootX;
        } else if (rank[rootX] > rank[rootY]) {
            data[rootX] = rootY;
        } else {
            data[rootY] = rootX;
            rank[rootX] += 1;
        }
    }

    public boolean connected(int x, int y) {
        return find(x) == find(y);
    }

    public void print() {
        for (int value : data) {
            System.out.print(value + " - ");
        }
        System.out.println();
    }
}
