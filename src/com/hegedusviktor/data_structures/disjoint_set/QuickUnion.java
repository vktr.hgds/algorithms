package com.hegedusviktor.data_structures.disjoint_set;

public class QuickUnion {
    private int[] data;

    public QuickUnion(int size) {
        data = new int[size];
        for (int i = 0; i < data.length; i++) {
            data[i] = i;
        }
    }

    public int find(int x) {
        while (x != data[x]) {
            x = data[x];
        }
        return x;
    }

    public void union(int x, int y) {
        int rootX = find(x);
        int rootY = find(y);

        if (rootX == rootY) return;

        data[rootY] = rootX;
    }

    public boolean connected(int x, int y) {
        return find(x) == find(y);
    }

    public void print() {
        for (int value : data) {
            System.out.print(value + " - ");
        }
        System.out.println();
    }
}
