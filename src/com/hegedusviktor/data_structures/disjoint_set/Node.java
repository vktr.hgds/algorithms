package com.hegedusviktor.data_structures.disjoint_set;

public class Node {
    private int value;
    private boolean head;

    public Node(int value, boolean head) {
        this.value = value;
        this.head = head;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isHead() {
        return head;
    }

    public void setHead(boolean head) {
        this.head = head;
    }
}
