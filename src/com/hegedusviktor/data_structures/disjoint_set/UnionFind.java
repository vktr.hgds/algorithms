package com.hegedusviktor.data_structures.disjoint_set;

public class UnionFind {
    private int[] data;

    public UnionFind(int size) {
        data = new int[size];
        for (int i = 0; i < data.length; i++) {
            data[i] = i;
        }
    }

    public void union(int x, int y) {
        int rootX = data[x];
        int rootY = data[y];

        if (rootX == rootY) return;

        for (int i = 0; i < data.length; i++) {
            if (data[i] == rootY)
                data[i] = rootX;
        }
    }

    public boolean connected(int x, int y) {
        return data[x] == data[y];
    }

    public void print() {
        for (int value : data) {
            System.out.print(value + " - ");
        }
        System.out.println();
    }
}
