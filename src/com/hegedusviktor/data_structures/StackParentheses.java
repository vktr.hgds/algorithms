package com.hegedusviktor.data_structures;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class StackParentheses {

    public static void printValid() {
        try {
            File parenthesesTextFile = new File("/Users/hegedusviktor/Courses/Algorithms/src/com/hegedusviktor/parenthesesString.txt");
            Scanner myReader = new Scanner(parenthesesTextFile);

            while (myReader.hasNextLine()) {
                String currentRow = myReader.nextLine();
                char[] chars = currentRow.toCharArray();
                Stack<Character> parenthesesStack = new Stack<>();

                for (Character currentChar : chars) {
                    if (currentChar == '(' || currentChar == '[' || currentChar == '{') {
                        parenthesesStack.push(currentChar);
                    } else if (currentChar == ')') {
                        if (parenthesesStack.empty() || parenthesesStack.peek() != '(') {
                            System.out.println("false");
                            break;
                        }
                        parenthesesStack.pop();
                    } else if (currentChar == ']') {
                        if (parenthesesStack.empty() || parenthesesStack.peek() != '[') {
                            System.out.println("false");
                            break;
                        }
                        parenthesesStack.pop();
                    } else if (currentChar == '}') {
                        if (parenthesesStack.empty() || parenthesesStack.peek() != '{') {
                            System.out.println("false");
                            break;
                        }
                        parenthesesStack.pop();
                    }
                }
                System.out.println(parenthesesStack.empty() ? "true" : "false");
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
