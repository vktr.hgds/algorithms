package com.hegedusviktor.data_structures.binary_tree;

public class BinaryTree {

    public Node root;
    public int size = 0;

    public void add(int value) {
        Node current = new Node();
        current.value = value;
        current.left = null;
        current.right = null;

        if (root == null) {
            root = current;
            return;
        }

        Node tmp = root;

        while (true) {
            if (current.value <= tmp.value) {
                if (tmp.left == null)
                    break;

                tmp = tmp.left;
            } else {
                if (tmp.right == null)
                    break;

                tmp = tmp.right;
            }
        }

        if (current.value <= tmp.value) {
            tmp.left = current;
        } else {
            tmp.right = current;
        }
    }

    public void preOrder(Node node) {
        if (node == null) return;
        System.out.print(node.value + " | ");
        preOrder(node.left);
        preOrder(node.right);
    }

    public void inOrder(Node node) {
        if (node == null) return;
        preOrder(node.left);
        System.out.print(node.value + " | ");
        preOrder(node.right);
    }

    public void postOrder(Node node) {
        if (node == null) return;
        preOrder(node.left);
        preOrder(node.right);
        System.out.println(node.value + " - ");
    }

    static class Node {
        Node left;
        Node right;
        int value;
    }
}
