package com.hegedusviktor.search;

import com.hegedusviktor.common.CommonOperations;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

/**
 * only works on sorted list
 * O(log(n))
 * space complexity: O(log(n)) if recursive (fucntion call stack)
 */

public class BinarySearch implements ISearch{
    private int[] values;
    private int target;

    public BinarySearch(int[] values) {
        this.values = values;
    }
    public BinarySearch(int length, int target) {
        this.values = CommonOperations.generateRandomValues(length);
        this.target = target;
    }

    @Override
    public int search() {
        int left = 0;
        int right = values.length - 1;

        Arrays.sort(values);
        while (left <= right) {
            int middle = (left + right) / 2;
            if (values[middle] == target) return middle;
            if (target < values[middle]) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }

        return -1;
    }

    public int[] getValues() {
        return values;
    }

}
