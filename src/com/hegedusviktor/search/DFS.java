package com.hegedusviktor.search;

import java.util.HashSet;

public class DFS {
    public static int provinces(int[][] graph) {
        HashSet<Integer> indexes = new HashSet<>();
        int provinceCount = 0;

        for (int i = 0; i < graph.length; i++) {
            if (indexes.contains(i)) continue;

            dfs(graph, i, indexes);
            provinceCount++;
        }

        return provinceCount;
    }

    public static void dfs(int[][] graph, int i, HashSet<Integer> indexes) {
        indexes.add(i);

        for (int k = 0; k < graph.length; k++) {
            if (graph[i][k] == 1 && !indexes.contains(k)) {
                dfs(graph, k, indexes);
            }
        }
    }
}
