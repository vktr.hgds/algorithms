package com.hegedusviktor.search;

import com.hegedusviktor.common.CommonOperations;

import java.util.Arrays;

/**
 * divide into buckets, and linear search there, O(1) if first, O(n) if last
 * ideal block size = squareRoot(n)
 * works only on sorted list
 */

public class ExponentialSearch implements ISearch{
    private int[] values;
    private int target;

    public ExponentialSearch(int[] values) {
        this.values = values;
    }
    public ExponentialSearch(int length, int target) {
        this.values = CommonOperations.generateRandomValues(length);
        this.target = target;
    }

    @Override
    public int search() {
        Arrays.sort(values);
        int bound = 1;
        while (bound < values.length && values[bound] <= target)
            bound *= 2;

        int left = 0;
        int right = Math.min(bound, values.length - 1);

        while (left <= right) {
            int middle = (left + right) / 2;
            if (values[middle] == target)
                return middle;
            if (values[middle] < target)
                left = middle + 1;
            else
                right = middle - 1;
        }

        return -1;
    }

    public int[] getValues() {
        return values;
    }
}
