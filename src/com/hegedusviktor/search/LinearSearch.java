package com.hegedusviktor.search;

import com.hegedusviktor.common.CommonOperations;

/**
 * linear search, O(1) if first, O(n) if last
 */

public class LinearSearch implements ISearch{
    private int[] values;
    private int item;

    public LinearSearch(int[] values) {
        this.values = values;
    }
    public LinearSearch(int length, int item) {
        this.values = CommonOperations.generateRandomValues(length);
        this.item = item;
    }

    @Override
    public int search() {
        for (int i : values) {
            if (values[i] == item) return i;
        }
        return -1;
    }

    public int[] getValues() {
        return values;
    }

}
