package com.hegedusviktor.search;

import com.hegedusviktor.common.CommonOperations;

import java.util.Arrays;

/**
 * only works on sorted list
 * O(log3(n))
 * space complexity: O(log3(n)) if recursive (fucntion call stack)
 *
 * BUT: binary search is faster, since binary has less comparisons
 */

public class TernarySearch implements ISearch{
    private int[] values;
    private int target;

    public TernarySearch(int[] values) {
        this.values = values;
    }
    public TernarySearch(int length, int target) {
        this.values = CommonOperations.generateRandomValues(length);
        this.target = target;
    }

    @Override
    public int search() {
        Arrays.sort(values);
        int left = 0, right = values.length - 1;
        int partitionSize = (right - left) / 3;
        int mid1 = left + partitionSize;
        int mid2 = right - partitionSize;

        while (mid1 <= mid2) {
            partitionSize = (right - left) / 3;
            mid1 = left + partitionSize;
            mid2 = right - partitionSize;

            if (values[mid1] == target) return mid1;
            if (values[mid2] == target) return mid2;
            if (target < values[mid1]) right = mid1 - 1;
            else if (values[mid2] < target) left = mid2 + 1;
            else {
                left = mid1 + 1;
                right = mid2 - 1;
            }

        }

        return -1;
    }

    public int[] getValues() {
        return values;
    }
}
