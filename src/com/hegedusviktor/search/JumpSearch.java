package com.hegedusviktor.search;

import com.hegedusviktor.common.CommonOperations;

import java.util.Arrays;

/**
 * divide into buckets, and linear search there, O(1) if first, O(n) if last
 * ideal block size = squareRoot(n)
 * works only on sorted list
 */

public class JumpSearch implements ISearch{
    private int[] values;
    private int target;

    public JumpSearch(int[] values) {
        this.values = values;
    }
    public JumpSearch(int length, int target) {
        this.values = CommonOperations.generateRandomValues(length);
        this.target = target;
    }

    @Override
    public int search() {
        Arrays.sort(values);
        int blockSize = (int) Math.sqrt(values.length);
        int start = 0;
        int next = blockSize;

        while(start < values.length && values[next - 1] < target) {
            start = next;
            next += blockSize;
            if (next >= values.length) next = values.length;
        }

        for (int i = start; i < next; i++)
            if (values[i] == target)
                return i;

        return -1;
    }

    public int[] getValues() {
        return values;
    }
}
