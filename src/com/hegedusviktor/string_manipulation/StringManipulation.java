package com.hegedusviktor.string_manipulation;

import com.hegedusviktor.common.CommonOperations;

import java.util.*;

public class StringManipulation {
    public String text;

    public StringManipulation(String text) {
        this.text = text;
    }

    public int countVowels() {
        int counter = 0;
        char[] vowels = new char[] {'a', 'e', 'o', 'u', 'i'};

        for (int i = 0; i < text.length(); i++) {
            for (char vowel : vowels) {
                if (text.charAt(i) == vowel) {
                    counter++;
                    break;
                }
            }
        }

        return counter;
    }

    public String reverseString() {
        StringBuilder reversedString = new StringBuilder();

        for (int i = text.length() - 1; i >= 0; i--)
            reversedString.append(text.charAt(i));

        return reversedString.toString();
    }

    public String reverseWords() {
        List<String> words = new ArrayList<>();
        StringBuilder word = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) != ' ') word.append(text.charAt(i));
            if (text.charAt(i) == ' ' || i == text.length() - 1) {
                words.add(word.toString());
                word = new StringBuilder();
            }
        }

        StringBuilder reversedString = new StringBuilder();
        for (int i = words.size() - 1; i >= 0; i--) {
            reversedString.append(words.get(i));
            if (i != 0) reversedString.append(" ");
        }

        return reversedString.toString();
    }

    public boolean isRotation(String otherString) {
        return text.length() == otherString.length() && (text + text).contains(otherString);
    }

    public String reduceToSingleChars() {
        // Set.of(text).toString()
        Set<Character> countCharacters = new HashSet<>();

        for (int i = 0; i < text.length(); i++)
            countCharacters.add(text.charAt(i));

        StringBuilder textWithoutDuplicates = new StringBuilder();

        for (Character character : countCharacters)
            textWithoutDuplicates.append(character);

        return textWithoutDuplicates.toString();
    }

    public String mostRepeatedCharacter() {
        Map<Character, Integer> countCharacters = new HashMap<>();

        for (int i = 0; i < text.length(); i++)
            if (countCharacters.get(text.charAt(i)) != null)
                countCharacters.put(text.charAt(i), countCharacters.get(text.charAt(i)) + 1);
            else
                countCharacters.put(text.charAt(i), 1);

        int max = Integer.MIN_VALUE;
        String mostRepeatedChar = null;

        for (Map.Entry<Character, Integer> character : countCharacters.entrySet()) {
            if (max < character.getValue()) {
                max = character.getValue();
                mostRepeatedChar = character.getKey().toString();
            }
        }

        return mostRepeatedChar;
    }

    public String capitalize() {
        StringBuilder truncatedText = new StringBuilder();

        int i = 0;
        while (text.charAt(i) == ' ')
            i++;

        for (int j = i; j < text.length(); ++j) {
            if (text.charAt(j) != ' ' || (text.charAt(j) == ' ' && text.charAt(j + 1) != ' ' && j + 1 < text.length()))
                truncatedText.append(text.charAt(j));
        }

        StringBuilder capitalizedText = new StringBuilder();
        int capitalizeAsciiValue = 32;

        for (int k = 0; k < truncatedText.length(); ++k) {
            if (k == 0 || (truncatedText.charAt(k) != ' ' && truncatedText.charAt(k - 1) == ' '))
                capitalizedText.append((char) (truncatedText.charAt(k) - capitalizeAsciiValue));
            else
                capitalizedText.append(truncatedText.charAt(k));
        }

        return capitalizedText.toString();
    }

    public boolean isPalindrome() {
        for (int i = 0; i < text.length() / 2; i++)
            if (text.charAt(i) != text.charAt(text.length() - 1 - i))
                return false;

        return true;
    }
}
