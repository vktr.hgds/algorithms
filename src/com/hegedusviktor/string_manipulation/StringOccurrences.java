package com.hegedusviktor.string_manipulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class StringOccurrences {
    public static List<Integer> matchingStrings(List<String> strings, List<String> queries) {
        Map<String, Integer> occurrences = new LinkedHashMap<>();

        for (String q : queries) {
            occurrences.put(q, 0);
        }

        for (String s : strings) {
            for (String q : queries) {
                if (q.equals(s)) occurrences.put(q, occurrences.get(q) + 1);
            }
        }

        return new ArrayList<>(occurrences.values());
    }

    public static List<Integer> matchingStrings(String stringPath, String queryPath) {
            ArrayList<String> strings = addFileContentToList(stringPath);
            ArrayList<String> queries = addFileContentToList(queryPath);

            if (strings == null || queries == null) return null;
            if (strings.isEmpty() || queries.isEmpty()) return null;

            ArrayList<Integer> results = new ArrayList<>();
            Map<String, Integer> occurrences = new LinkedHashMap<>();

            for (String s : strings)
                occurrences.put(s, occurrences.getOrDefault(s, 0) + 1);

            for (String q : queries)
                results.add(occurrences.getOrDefault(q, 0));

            return results;
    }

    private static ArrayList<String> addFileContentToList(String path) {
        ArrayList<String> strings = new ArrayList<>();

        try {
            File rows = new File(path);
            Scanner scanner = new Scanner(rows);

            while (scanner.hasNextLine())
                strings.add(scanner.nextLine());

            return strings;
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
            return null;
        }
    }
}
